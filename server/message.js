let { Message, Dialog, DialogMembers } = require("../database/models")

module.exports = async (req, res) => {
  // проверка, есть ли в запросе message_id
  if(!req.query["message_id"]){
    res.send({ "error": "message id expected" })
    res.status(400)
    return
  }
  const messageId = parseInt(req.query["message_id"])

  // проверка, существует ли такое сообщение
  const message = await Message.findOne({
    where: {
      id: messageId
    }
  })

  // если диалог не сущесвует, выдаем ошибку
  if (!message) {
    res.status(404)
    return res.send({ "error": "message not found" })
  }

  // проверка, является ли пользователь участником диалога
  const isMember = await DialogMembers.findOne({
    where: {
      dialogId: message.dialogId,
      userId: req.user.id
    }
  })

  // если не является, то выдаем ошибку доступа
  if (!isMember) {
    res.status(403)
    return res.send({ "error": "access denied" })
  }

  res.send(message)
}