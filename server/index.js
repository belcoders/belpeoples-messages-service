module.exports = (app, io) => {
  app.use(require("./middleware"))

  app.get("/send", (req, res) => {
    require("./send")(req, res, io)
  })
  app.get("/dialog", require("./dialog"))
  app.get("/message", require("./message"))
  app.get("/list", require("./list"))
}