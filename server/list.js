const axios = require("axios")
const { Op } = require("sequelize");
let { Message, DialogMembers, Dialog } = require("../database/models")

module.exports = async (req, res) => {
  // берем все диалоги пользователя
  const dialogs = []
  const dialogsDb = await DialogMembers.findAll({
    where: {
      userId: req.user.id
    }
  })

  for(let dialog of dialogsDb) {
    try {
      // берем последнее сообщение из диалога
      const dialogLastMessage = await Message.findOne({
        where: {
          userId: req.user.id
        },
        order: [
          ["id", "DESC"],
        ],
      })

      // берем пользователя, который отправил последнее сообщение
      const response = await axios.get(process.env.VUE_APP_USERS_API + "users/" + dialogLastMessage.userId + "?access_token=" + req.token)

      let dialogName = ""
      let senderName = ""
      // берем из базы объект с диалогом
      const dialogDb = await Dialog.findOne({
        where: {
          id: dialog.dialogId
        }
      })

      // если тип равняется одному (личные сообщения, не беседа), то ...
      if (dialogDb.type === 1) {
        // берем второго участника диалога
        const dialogAnotherMember = await DialogMembers.findOne({
          where: {
            [Op.and]: [
              { dialogId: dialog.dialogId },
            ], [Op.not]: [
              { userId: req.user.id }
            ]
          }
        })

        const response = await axios.get(process.env.VUE_APP_USERS_API + "users/" + dialogAnotherMember.userId + "?access_token=" + req.token)
        // устанавливаем имя пользователя как название диалога
        dialogName = response.data.response.first_name + " " + response.data.response.last_name

        // устанавливаем имя отправителя
        if(dialogLastMessage.userId === req.user.id) senderName = "Вы"
        else senderName = response.data.response.first_name
      }

      // добавляем информацию о диалоге в выдачу
      dialogs.push({
        "name": dialogName,
        "sender": senderName,
        "avatar": "dialog avatar",
        "last_message": dialogLastMessage,
        "last_message_user": response.data.response
      })
    } catch(err) {
      console.log(err)
    }
  }

  res.send(dialogs)
}