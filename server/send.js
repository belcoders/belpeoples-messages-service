const axios = require("axios")
let { Message, Dialog, DialogMembers } = require("../database/models")
let sockets = require("../socket/data")

module.exports = async (req, res, io) => {
  // проверка, есть ли в запросе user_id
  if(!req.query["user_id"]){
    res.send({ "error": "user id expected" })
    res.status(400)
    return
  }
  const userId = parseInt(req.query["user_id"])

  // проверка, есть ли в запросе text
  if(!req.query["text"]){
    res.send({ "error": "message text expected" })
    res.status(400)
    return
  }
  const messageText = req.query["text"]

  try {
    // проверка, существует ли получатель
    const response = await axios.get(process.env.VUE_APP_USERS_API + "users/" + userId + "?access_token=" + req.token)

    if(!req.query["dialog_id"]){
      // есть в запросе нет dialog_id ...
      // берем все диалоги отправителя
      const dialogsMemberSender = await DialogMembers.findAll({
        where: {
          userId: req.user.id
        }
      })
      // берем все диалоги получателя
      const dialogsMemberRecipient = await DialogMembers.findAll({
        where: {
          userId: userId
        }
      })
      // номер их диалога
      let dialogId = 0

      // пробежка по диалогам отправителя
      for(let dialogSender of dialogsMemberSender){
        // пробежка по диалогам получателя
        for(let dialogRecipient of dialogsMemberRecipient){
          // если есть общий диалог, то записываем его в dialogId
          if(dialogSender.dialogId === dialogRecipient.dialogId) {
            // проверка, что диалог является личным
            const verifyDialog = await Dialog.findAll({
              where: {
                id: dialogSender.dialogId,
                type: 1
              }
            })
            if(verifyDialog.length > 0){
              dialogId = dialogSender.dialogId
            }
          }
        }
      }

      if(dialogId !== 0) {
        // если есть общий диалог ...
        // создаем новое сообщение в существующем диалоге
        const message = await Message.create({
          dialogId: dialogId,
          userId: req.user.id,
          text: messageText
        })
      } else {
        // если нет общего диалога ...
        // создаем новый диалог
        const newDialog = await Dialog.create({
          type: 1
        })
        // добавляем в диалог отправителя
        const newDialogMemberSender = await DialogMembers.create({
          dialogId: newDialog.id,
          userId: req.user.id
        })
        // добавляем в диалог получателя
        const newDialogMemberRecipient = await DialogMembers.create({
          dialogId: newDialog.id,
          userId: userId
        })

        // создаем новое сообщение в новом диалоге
        const message = await Message.create({
          dialogId: newDialog.id,
          userId: req.user.id,
          text: messageText
        })
      }
    } else {
      // если в запросе есть dialog_id ...
    }

    // проверяем есть ли подключенные сокеты у пользователя
    if(sockets[userId]) {
      // если есть ...
      // пробегаем по списку подключенных сокетов пользователя
      for (let socket of sockets[userId]) {
        // отправляем каждому сокету новое сообщение
        io.to(socket).emit("newMessage", {
          text: messageText
        })
      }
    }

    res.status(201)
    res.send({ "message": "message sent"})
  } catch (err) {
    if(err.response) {
      if(err.response.status === 404){
        // ошибка, если получатель не найден
        res.status(400)
        return res.send({ "error": "Пользователь не найден." })
      } else {
        // другие ошибки...
        console.log(err)
        res.status(500)
        return res.send({ "error": "server error" })
      }
    } else {
      // другие ошибки...
      console.log(err)
      res.status(500)
      return res.send({ "error": "server error" })
    }
  }
}