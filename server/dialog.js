let { Message, Dialog, DialogMembers } = require("../database/models")

module.exports = async (req, res) => {
  // проверка, есть ли в запросе dialog_id
  if(!req.query["dialog_id"]){
    res.send({ "error": "dialog id expected" })
    res.status(400)
    return
  }
  const dialogId = parseInt(req.query["dialog_id"])

  // максимальное количество возвращаемых сообщений
  let limit = 20
  if(req.query["limit"]){
    limit = parseInt(req.query["limit"])
    if (limit > 20) limit = 20
  }

  // начать с сообщения
  let offset = 0
  if(req.query["offset"]){
    offset = parseInt(req.query["offset"])
  }

  // проверка, существует ли такой диалог
  const dialog = await Dialog.findOne({
    where: {
      id: dialogId
    }
  })

  // если диалог не сущесвует, выдаем ошибку
  if (!dialog) {
    res.status(404)
    return res.send({ "error": "dialog not found" })
  }

  // проверка, является ли пользователь участником диалога
  const isMember = await DialogMembers.findOne({
    where: {
      dialogId: dialog.id,
      userId: req.user.id
    }
  })

  // если не является, то выдаем ошибку доступа
  if (!isMember) {
    res.status(403)
    return res.send({ "error": "access denied" })
  }

  // берем все сообщения из данного диалога с лимитом limit и оффсетом offset с сортировкой по ID
  const messages = await Message.findAll({
    where: {
      dialogId: dialog.id
    },
    order: [
      ["id", "DESC"],
    ],
    limit: limit,
    offset: offset
  })

  res.send(messages)
}