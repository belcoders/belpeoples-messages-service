const axios = require("axios")

module.exports = (req, res, next) => {
  // проверяем, есть ли в запросе access_token
  if(!req.query["access_token"]){
    res.send({ "error": "access token expected" })
    return res.status(400)
  }
  const accessToken = req.query["access_token"]

  // проверяем, существует ли такой токен
  axios.get(process.env.VUE_APP_USERS_API + "auth/account?access_token=" + accessToken).then(response => {
    // если да, то берем пользователя и токен и записываем их в запрос
    req.user = response.data.user
    req.token = accessToken
    next()
  }).catch(error => {
    if(error.response.status === 401){
      // если нет, то выдаем ошибку, что токена не существует
      res.status(401)
      return res.send({ "error": "token not found" })
    } else {
      // обработка остальных ошибок
      console.log(error.response)
      res.status(500)
      return res.send({ "error": "server error" })
    }
  })
}