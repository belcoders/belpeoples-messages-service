const axios = require("axios")

module.exports = io => {
  io.use((socket, next) => {
    try {
      // берем из запроса сокета токен
      const { token } = socket.handshake.query

      // проверяем, сущесвует ли такой токен
      axios.get(process.env.VUE_APP_USERS_API + "auth/account?access_token=" + token).then(response => {
        // если да, то удаляем пароль и записываем пользователя в запрос сокета
        let user = response.data.user
        delete user.password
        socket.request.user = user
        return next()
      })
    } catch (err) {
      // если нет, то игнорируем
      return next()
    }
  })
}