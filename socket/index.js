let authMiddleware = require("./auth")
let listen = require("./listen")

module.exports = io => {
  authMiddleware(io)
  listen(io)
}