let sockets = require("./data")

module.exports = io => {
  // чистка токенов каждые 10 минут
  setInterval(() => {
    // пробегаемся по всем пользователям
    for(let userSocket in sockets){
      // пробегаемся по всем сокетам пользователя
      for (let socket of sockets[userSocket]) {
        // если сокет не подключен, то удаляем его
        if(!io.sockets.sockets[socket]){
          delete io.sockets.sockets[socket]
        }
      }
    }
  }, 600000)

  io.on("connection", socket => {
    // проверяем, есть ли в объекте массив с сокетами пользователя
    if(!sockets[socket.request.user.id]){
      // если нет, то создаем его
      sockets[socket.request.user.id] = []
    }
    // добавляем подключенный сокет в массив с сокетами пользователя
    sockets[socket.request.user.id].push(socket.id)
    socket.on("disconnect", () => {
      // при отключении проверяем, есть ли в объекте массив с сокетами пользователя
      if(sockets[socket.request.user.id]) {
        // если есть, то находим отключившейся сокет в массиве
        let index = sockets[socket.request.user.id].indexOf(socket.id)
        if(index > -1) {
          // удаляем отключившейся сокет
          sockets[socket.request.user.id].splice(index, 1)
        }
      }
    })
  })
}