'use strict';
module.exports = (sequelize, DataTypes) => {
  const DialogMembers = sequelize.define('DialogMembers', {
    dialogId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER
  }, {});
  DialogMembers.associate = function(models) {
    // associations can be defined here
  };
  return DialogMembers;
};