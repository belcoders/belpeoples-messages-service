'use strict';
module.exports = (sequelize, DataTypes) => {
  const Dialog = sequelize.define('Dialog', {
  	type: DataTypes.INTEGER
  }, {});
  Dialog.associate = function(models) {
    // associations can be defined here
  };
  return Dialog;
};