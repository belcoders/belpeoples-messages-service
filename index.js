require("dotenv").config()
let app = require("express")()
let http = require("http").createServer(app)
let cors = require("cors")
let io = require("socket.io")(http)
let loadIO = require("./socket")
let loadServer = require("./server")

// подключаем CORS
app.use(cors())

loadIO(io)
loadServer(app, io)

// берем порт из переменных окружения, если нет, то принимаем порт равным 8080
const port = process.env.APP_PORT || 8080

// запускаем сервер
http.listen(port, () => {
  console.log("listening on *:" + port)
})